

function sumarUno(numero){

	var promesa = new Promise( function(resolve,reject){

		if(numero >= 7){
			reject("Error");
		}

		setTimeout( function(){
			resolve( numero + 1 );
			console.log(numero);
		},800);

	});

	return promesa;
}


sumarUno(5)
.then( sumarUno )
.then( sumarUno )
.then( sumarUno )
.catch( error => {
	console.log("Error en Promesa");
	console.log(error);
});