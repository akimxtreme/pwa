console.log("fetch-1.js");

var url = 'https://reqres.in/api/users';


function get(response) {
	console.log(response.data);
	
	response.data.forEach(function(element) {
	  console.log(element);
	});

}	

async function getUserAsync(url) 
{
  let response = await fetch(url);
  let data = await response.json()
  return data;
}

getUserAsync(url)
  .then(data => get(data)); 